package Rechnung.dao;

import Rechnung.model.Rechnung;

import java.sql.SQLException;
import java.util.List;

public interface RechnungDAO {
    //CRUD methods

    //create Rechnung
    void createRechnung(Rechnung rechnung) throws SQLException;

    //update Rechnung
    boolean updateRechnung(Rechnung rechnung, int id) throws SQLException;

    //select Rechnung by ID
    Rechnung selectRechnung(int id);

    //select alle Rechnungen
    List<Rechnung> selectAllRechnung();

    //delete Rechnung
    boolean deleteRechnung(int id) throws SQLException;

}
