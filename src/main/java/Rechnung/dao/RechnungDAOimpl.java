package Rechnung.dao;


import Rechnung.model.Rechnung;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RechnungDAOimpl implements RechnungDAO {

    //Variables for Connection
    private String jdbcURL = "jdbc:mysql://127.0.0.1:3306/Rechnungen";
    private String jdbcUsername = "root";
    private String jdbcPassword = "1234";

    //SQL Befehle

    private static final String INSERT_RECHNUNG_SQL  = "INSERT INTO Rechnung" + "(datum, description, wert, bezahlt) VALUES " + " (?, ?, ?, ?);";
    private static final String SELECT_RECHNUNG_BY_ID  = "SELECT id, datum, description, wert, bezahlt FROM Rechnung where id =?;";
    private static final String SELECT_ALL_RECHNUNGEN  = "SELECT * FROM Rechnung;";
    private static final String DELETE_RECHNUNGEN_SQL  = "DELETE FROM Rechnung WHERE id = ?;";
    private static final String UPDATE_RECHNUNGEN_SQL  = "UPDATE Rechnung SET datum = ?, description = ?, wert = ?, bezahlt=? WHERE id = ?;";

    public RechnungDAOimpl() {

    }


    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    


    @Override
    public void createRechnung(Rechnung rechnung) throws SQLException {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_RECHNUNG_SQL)) {
            preparedStatement.setDate(1, Date.valueOf(rechnung.getDate().toString()));
            preparedStatement.setString(2, rechnung.getDescription());
            preparedStatement.setDouble(3, rechnung.getWert());
            preparedStatement.setBoolean(4, rechnung.isBezahlt());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
        }
    }



    @Override
    public boolean updateRechnung(Rechnung rechnung, int id) throws SQLException {
        boolean rowUpdated = false;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_RECHNUNGEN_SQL)) {
            preparedStatement.setDate(1, (Date) rechnung.getDate());
            preparedStatement.setString(2, rechnung.getDescription());
            preparedStatement.setDouble(3, rechnung.getWert());
            preparedStatement.setBoolean(4, rechnung.isBezahlt());
            preparedStatement.setInt(5, id);
            rowUpdated = preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
        }
        return rowUpdated;
    }

    @Override
    public Rechnung selectRechnung(int id) {
        Rechnung rechnung = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_RECHNUNG_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                Date date = rs.getDate("datum");
                String description = rs.getString("description");
                Double wert = rs.getDouble("wert");
                Boolean bezahlt = rs.getBoolean("bezahlt");
                rechnung = new Rechnung(date, description,wert,bezahlt);
            }

        } catch (SQLException e) {
        }
        return rechnung;
    }

    @Override
    public List<Rechnung> selectAllRechnung() {
        List<Rechnung> rechnungList = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_RECHNUNGEN)) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                Date date = rs.getDate("datum");
                String description = rs.getString("description");
                Double wert = rs.getDouble("wert");
                boolean bezahlt = rs.getBoolean("bezahlt");
                rechnungList.add(new Rechnung(date, description,wert,bezahlt));
            }

        } catch (SQLException e) {
        }
        return rechnungList;
    }

    @Override
    public boolean deleteRechnung(int id) throws SQLException {
        boolean rowDeledet = false;
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_RECHNUNGEN_SQL)) {
            preparedStatement.setInt(1,id);
            rowDeledet= preparedStatement.executeUpdate()>0;
        }
        return rowDeledet;
    }
}
