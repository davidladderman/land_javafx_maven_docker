package Rechnung.model;

import java.sql.Date;

public class Rechnung {
    private Integer id;
    private Date date;
    private String description;
    private Double wert;
    private boolean bezahlt;

    public Rechnung(Date date, String description, Double wert, boolean bezahlt) {
        this.date = date;
        this.description = description;
        this.wert = wert;
        this.bezahlt = bezahlt;

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getWert() {
        return wert;
    }

    public void setWert(Double wert) {
        this.wert = wert;
    }

    public boolean isBezahlt() {
        return bezahlt;
    }

    public void setBezahlt(boolean bezahlt) {
        this.bezahlt = bezahlt;
    }


}
