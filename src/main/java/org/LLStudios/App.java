package org.LLStudios;

import Rechnung.dao.RechnungDAO;
import Rechnung.dao.RechnungDAOimpl;
import Rechnung.model.Rechnung;
import com.mysql.cj.util.Util;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"));
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) throws SQLException {
        //Teste Alle Rechnungen ausgeben
        RechnungDAO ra = new RechnungDAOimpl();
        ArrayList<Rechnung> rl = new ArrayList<>();
        rl.addAll(ra.selectAllRechnung());
        for (Rechnung r:rl) {
            System.out.println(r.getId());
            System.out.println(r.getDescription());
            System.out.println(r.getDate());
            System.out.println(r.getWert());
            System.out.println(r.isBezahlt());
        }
        //Teste Create neue Rechnung erstellen
        java.util.Date utilDate = new java.util.Date();
        Rechnung r1 = new Rechnung(new java.sql.Date(utilDate.getTime()),"Yugioh Karten",12.2,false);
        ra.createRechnung(r1);

        //Teste einzelen REchnugn anzeigen
        Rechnung r2 = ra.selectRechnung(2);
        System.out.println(r2.getDescription() + " " + r2.getId());

        //Teste Update
        Rechnung r3 = new Rechnung(new java.sql.Date(utilDate.getTime()),"Geuptatet Test",12.2,false);
        ra.updateRechnung(r3,1);

        //Teste Delete
        ra.deleteRechnung(0);


        launch();

    }

}